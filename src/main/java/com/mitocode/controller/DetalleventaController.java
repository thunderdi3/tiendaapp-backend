package com.mitocode.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.model.Detalleventa;
import com.mitocode.service.IDetalleventaService;

@RestController
@RequestMapping("/detalleventa")
public class DetalleventaController {

	@Autowired
	private IDetalleventaService service;

	@GetMapping(value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Detalleventa>> listar() {
		List<Detalleventa> Detalleventas = new ArrayList<>();
		try {
			Detalleventas = service.listar();
		} catch (Exception e) {
			return new ResponseEntity<List<Detalleventa>>(Detalleventas, HttpStatus.OK);
		}

		return new ResponseEntity<List<Detalleventa>>(Detalleventas, HttpStatus.OK);
	}

	@PostMapping(value = "/registrar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Detalleventa> registrar(@RequestBody Detalleventa Detalleventa) {
		Detalleventa auxDetalleventa = new Detalleventa();
		try {
			auxDetalleventa = service.registrar(Detalleventa);
		} catch (Exception e) {
			return new ResponseEntity<Detalleventa>(auxDetalleventa, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Detalleventa>(auxDetalleventa, HttpStatus.OK);
	}
	
	
	
}

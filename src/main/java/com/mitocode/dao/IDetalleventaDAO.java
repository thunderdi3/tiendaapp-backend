package com.mitocode.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mitocode.model.Detalleventa;

@Repository
public interface IDetalleventaDAO extends JpaRepository<Detalleventa, Integer>{

}

package com.mitocode.service;

import java.util.List;

import com.mitocode.model.Detalleventa;

public interface IDetalleventaService {

	// CREAR Y LISTAR

	Detalleventa registrar(Detalleventa detalleventa);

	List<Detalleventa> listar();

}

package com.mitocode.service;

import java.util.List;

import com.mitocode.model.Producto;

public interface IProductoService {

	//CRUD
	Producto registrar(Producto producto);

	void modificar(Producto producto);

	void eliminar(int idProducto);

	Producto listarId(int idProducto);

	List<Producto> listar();
}

package com.mitocode.service;

import java.util.List;

import com.mitocode.model.Venta;

public interface IVentaService {

	// CREAR Y LISTAR

	Venta registrar(Venta venta);

	List<Venta> listar();

}

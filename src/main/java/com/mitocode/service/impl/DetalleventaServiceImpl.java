package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.IDetalleventaDAO;
import com.mitocode.model.Detalleventa;
import com.mitocode.service.IDetalleventaService;

@Service
public class DetalleventaServiceImpl implements IDetalleventaService {

	@Autowired
	private IDetalleventaDAO dao;

	@Override
	public Detalleventa registrar(Detalleventa detalleventa) {
		return dao.save(detalleventa);
	}

	@Override
	public List<Detalleventa> listar() {
		return dao.findAll();
	}

}

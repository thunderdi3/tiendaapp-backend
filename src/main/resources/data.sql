INSERT INTO public.persona( apellidos, nombres) VALUES ( 'Santiago', 'Hernandez');
INSERT INTO public.persona( apellidos, nombres) VALUES ( 'Estefania', 'Sandoval');
INSERT INTO public.persona( apellidos, nombres) VALUES ( 'Patricia', 'Ortega');
INSERT INTO public.persona( apellidos, nombres) VALUES ( 'Belen', 'Torres');
INSERT INTO public.persona( apellidos, nombres) VALUES ( 'Oswaldo', 'Guayasamin');

INSERT INTO public.producto( marca, nombre)VALUES ( 'Televisor 32"', 'Sony');
    INSERT INTO public.producto( marca, nombre)VALUES ( 'Iphone X', 'Apple');
    INSERT INTO public.producto( marca, nombre)VALUES ( 'Watch', 'Apple');
    INSERT INTO public.producto( marca, nombre)VALUES ( 'Laptop X Alienware', 'Alienware');
    INSERT INTO public.producto( marca, nombre)VALUES ( 'Mouse GTD', 'Microsoft');
    INSERT INTO public.producto( marca, nombre)VALUES ( 'Teclado bluetooth', 'Logitech');
    INSERT INTO public.producto( marca, nombre)VALUES ( 'Mouse G602', 'Logitech');
    
    
    INSERT INTO public.venta( fecha, importe, id_persona)VALUES ( '2018-04-19 10:00:00.286', 234.50, 1);
    INSERT INTO public.venta( fecha, importe, id_persona)VALUES ( '2018-04-19 10:20:20.286', 1540, 2);
    INSERT INTO public.venta( fecha, importe, id_persona)VALUES ( '2018-04-19 11:00:40.286', 80, 3);
    INSERT INTO public.venta( fecha, importe, id_persona)VALUES ( '2018-04-19 11:30:30.286', 3200.83, 4);
    
    INSERT INTO public.detalleventa( cantidad, id_producto, id_venta)VALUES ( 5, 6, 1);
    INSERT INTO public.detalleventa( cantidad, id_producto, id_venta)VALUES ( 1, 2, 2);
    INSERT INTO public.detalleventa( cantidad, id_producto, id_venta)VALUES ( 8, 7, 3);
    INSERT INTO public.detalleventa( cantidad, id_producto, id_venta)VALUES ( 1, 2, 3);
    INSERT INTO public.detalleventa( cantidad, id_producto, id_venta)VALUES ( 1, 3, 3);
    INSERT INTO public.detalleventa( cantidad, id_producto, id_venta)VALUES ( 1, 4, 3);